
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";


import React ,{Suspense} from 'react';
import Loader from './Loader/Loader';


const LoginPage = React.lazy(()=>import('./Component/LoginPage/LoginPage'))
function App() {
  return (
  <>

<Router>
      <div>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Suspense fallback={<div><Loader/></div>}>
          <Route exact path="/Login">
          <LoginPage />
          </Route>
          <Route  path="/">
            <Redirect to="/Login"></Redirect>
          </Route>
          </Suspense>
        </Switch>
      </div>
    </Router>

  </>
 );
}

export default App;
