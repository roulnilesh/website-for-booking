import axios from 'axios';

// const ourRequest = axios.CancelToken.source();

export async function getData(url) {
  let config = {
    method: 'get',
    withCredentials: true,
    url: url,
    onUploadProgress: progressEvent => {
      // console.log(progressEvent.loaded)
    },
    onDownloadProgress: ProgressEvent => {
      // console.log(ProgressEvent.loaded,ProgressEvent.total,ProgressEvent.currentTarget.response.length)
    },
  };
  let data;
  try {
    data = await axios(config);
    return data;
  } catch (error) {
    if (error.response) {
      // Request made and server responded
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }

    return error.toJSON();
  }
}
