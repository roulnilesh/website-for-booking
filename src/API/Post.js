import axios from 'axios';

const transport = axios.create({
  withCredentials: true,
});
const options = {
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json;charset=utf-8',
    Authorization: 'JWT fefege...',
  },
};

export async function postRequest(url, payload) {
  let data;
  console.log(options);
  try {
    data = await transport.post(url, payload, options);
    return data;
  } catch (error) {
    return error;
  }
}
