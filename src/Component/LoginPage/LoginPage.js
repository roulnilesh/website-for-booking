import "./LoginPage.css";
import logo from '../../images/logoCarnival.jpg';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Notification from '../Notification/Notification';
import { getData } from "../../API/Get";
import { observer } from "mobx-react-lite" 

import React, { useState ,useRef, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
// import LoginStore from "../../Store/LoginStore"


import FormControl from '@material-ui/core/FormControl';
import DashBoard from "../Dashboard/Dashboard";

const useStyles = makeStyles((theme) => ({
  input: {
    "&:invalid": {
      border: "red solid 2px"
    }
  },
  root: {
    '& > *': {


    },
  },
  formControl: {

    width: '100%'

  },
  selectEmpty: {
    marginTop: theme.spacing(2),
    right: 2
  },
}));
function LoginPage() {
  const classes = useStyles();
  let [userLoggedIn,setUserLoggedIn]= useState(false);
  let [type, setTypePax] = useState("");
  let cabin= useRef("");
  let password = useRef("");
  let category = useRef("");
  let [inputObj,setInputObj]=useState(
    {
      cabin:"C213",
      password:"19081992",
      category:""
    }
  )


  /**
   * COMPONENT DID MOUNT && COMPONENT DID UPDATE  && COMPOENTN DID UNMOUNT
   */

  useEffect(()=>{
    console.log("first")
  });

  /**
   * COMPIOENT DID MOUNT
   */

  useEffect(()=>{

  },[]);



  let [notification,setNotification]= useState({
    vertical:"top",
    horizontal:"center",
    severity:"",
    message:""
  })

  let [formObject, setFormObject] = useState({
    cabin: {
      bool:false,
      error:"Please enter  CabinNo  (XXXX)"
    },
    password: {
      bool:false,
      error:"Please enter DOB in DDMMYYYY"
    }
  })

  const selectTypeOfuser = (event) => {
    setTypePax(event.target.value);
    let obj={...inputObj};
    obj["category"]= event.target.value;
    setInputObj(obj)
  }


  const inputFeildChange = (type, event) => {
    let value = event.target.value;
   
    let obj = { ...formObject };

   
    
    if (type === "password") {
      let regExp = /[a-zA-Z]/g;
      if (regExp.test(value)) {
        obj[type]={
          bool:true,
          error:"Please enter only digits"
        }
             
      }
      else if(value.length < 8){
        obj[type]={
          bool:true,
          error:"Please enter DOB in DDMMYYYY"
        } 
      }
      else{
        obj[type]={
          bool:false,
          error:"Please enter DOB in DDMMYYYY"
        } 
        let inO={...inputObj};
        inO[type]= value;
        setInputObj(inO)
      }
    }
    else{
      let regExp =/^[a-z0-9]+$/i
      if (value.match(regExp) === null) {
        obj[type]={
          bool:true,
          error:"Please enter alphanumeric "
        }
             
      }
      else{
        if(value.length < 4){
          obj[type]={
            bool:true,
            error:"Please enter complete CabinNo in XXXX"
          } 
        }
        else{
          obj[type]={
            bool:false,
            error:"Please enter  CabinNo  (XXXX)"
          }
          let inO={...inputObj};
          inO[type]= value;
          setInputObj(inO)
        }
      }

    }
    
   
    
    setFormObject(obj)
    
  }



  const checkForFormData =async()=>{
    let obj = {...inputObj};
    let notifyObj= {...notification};

    let response = await getData("../JSONS/users.json");
    if(response.status < 300){
    let validUser = false;
    for(let i =0;i< response.data.length;i++){
      let objiterator= response.data[i];
      if(objiterator.id===obj.cabin && objiterator.password === obj.password && objiterator.category === obj.category ){
        validUser = true;
        setUserLoggedIn(true)
      }

    }
    if(!validUser){
      setNotification({
        ...notifyObj,
        message:"Please check your credentials",
        severity:"error"
    })
    }

    }else{
      setNotification({
        ...notifyObj,
        message:"Server error",
        severity:"error"
    }) 
    }
  
  
    
  }
  return (
<>
   {
     notification.message !==""?<Notification message={notification.message} severity={notification.severity} position={{horizontal:notification.horizontal,vertical:notification.vertical}} close={()=>setNotification({...notification,message:""})}/>:null
   }
{
  userLoggedIn ? <DashBoard/>:  <div className="forcenter" >
  <div className="identify-border row " >
    <div className=" col-md-12 image-container d-flex justify-content-center">

      <img className=" image-logo" src={logo} alt="" />
    </div>

    <div className=" col-md-12 input-container">
      <form className={classes.root} autoComplete="off">
        <div className="userinput w-40 mb-4">


          <TextField className="userinput w-40"   inputRef={cabin} value="C213"   error={formObject.cabin.bool} onChange={(e) => inputFeildChange("cabin", e)} fullWidth={true} id="myusername" label="Cabin No" variant="outlined" inputProps={{
            maxLength: 4,
          }}
            helperText={formObject.cabin.error} />
        </div>
        <div className="userinput w-40  mb-4">
          <TextField className="userinput " fullWidth={true}
            id="outlined-password-input"
            error={formObject.password.bool}
            onChange={(e) => inputFeildChange("password", e)}
            label="DOB"
            type="password"
            value="19081992"
            inputRef={password} 
           
            autoComplete="current-password"
            variant="outlined"
            inputProps={{
              maxLength: 8,
              pattern: "[a-z]"
            }}

            helperText={formObject.password.error}
          />


        </div>
        <div className="select-button w-40  mb-4 ">
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
            inputRef={category}
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              onChange={selectTypeOfuser}
              defaultValue="Category"
              className="select-box"
             
              variant="outlined"
              displayEmpty
              value={type}


            >

              <MenuItem value={"Crew"}>Crew</MenuItem>
              <MenuItem value={"Passenger"}>Passenger</MenuItem>

            </Select>
          </FormControl>
        </div>
        <div className="w-40 d-flex mt-2  mb-4">
          <div className="col-md-5 p-0 ">
            <Button variant="contained" disabled={inputObj.cabin === "" || inputObj.password === "" || inputObj.category === ""} fullWidth={true} onClick={checkForFormData} color="primary" >login</Button>
          </div>

          <div className=" col-md-5 p-0 ms-auto">
            <Button className="loginbutton" fullWidth={true} variant="contained" color="secondary">signup</Button>
          </div>


        </div>
        <div className="w-40 d-flex click-option mt-2  mb-4">
          <div className="col-md-5 d-flex justify-content-center align-items-center">
            <label className="remember-me" > Remember Me</label>
            <Checkbox
              defaultChecked
              color="primary"
              inputProps={{ 'aria-label': 'secondary checkbox' }}
            />

          </div>

          <div className="col-md-5 ms-auto d-flex justify-content-center align-items-center">
            <label className="remember-me"> Forget password?</label>
          </div>


        </div>
      </form>
    </div>

  </div>
</div>
 

}
  </>
  );
}
export default LoginPage;